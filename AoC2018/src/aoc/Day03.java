/**
 * 
 */
package aoc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayList<ArrayList<Integer>> input = new ArrayList<ArrayList<Integer>>();
		int maxX = 0;
		int maxY = 0;
		int[][] fabric = null;
		int overlap = 0;
		int intact = -1;
		try {

			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					throw new Exception();
				}

				ArrayList<Integer> current = new ArrayList<Integer>();
				current.add(Integer.valueOf(seq.substring(1, seq.indexOf("@")).trim()));
				current.add(Integer.valueOf(seq.substring(seq.indexOf("@") + 1, seq.indexOf(",")).trim()));
				current.add(Integer.valueOf(seq.substring(seq.indexOf(",") + 1, seq.indexOf(":")).trim()));
				current.add(Integer.valueOf(seq.substring(seq.indexOf(":") + 1, seq.indexOf("x")).trim()));
				current.add(Integer.valueOf(seq.substring(seq.indexOf("x") + 1).trim()));
				input.add(current);

				maxX = maxX > (current.get(1) + current.get(3)) ? maxX : (current.get(1) + current.get(3));
				maxY = maxY > (current.get(2) + current.get(4)) ? maxY : (current.get(2) + current.get(4));

			}

		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(e);
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}

		System.out.println("\n---------------------------------");
		System.out.println(":maxX> " + maxX);
		System.out.println(":maxY> " + maxY);

		fabric = new int[maxX][maxY];

		for (int i = 0; i < input.size(); i++) {
			int xStart = input.get(i).get(1);
			int xEnd = xStart + input.get(i).get(3);
			int yStart = input.get(i).get(2);
			int yEnd = yStart + input.get(i).get(4);

			for (int x = xStart; x < xEnd; x++) {
				for (int y = yStart; y < yEnd; y++) {
					fabric[x][y] = fabric[x][y] + 1;
				}
			}
		}

		for (int x = 0; x < maxX; x++) {
			for (int y = 0; y < maxY; y++) {
				overlap = fabric[x][y] > 1 ? overlap + 1 : overlap + 0;
			}
		}

		for (int i = 0; i < input.size(); i++) {
			int xStart = input.get(i).get(1);
			int xEnd = xStart + input.get(i).get(3);
			int yStart = input.get(i).get(2);
			int yEnd = yStart + input.get(i).get(4);

			boolean overlapDetected = false;
			for (int x = xStart; x < xEnd; x++) {
				for (int y = yStart; y < yEnd; y++) {
					overlapDetected = overlapDetected || (fabric[x][y] > 1);
				}
			}

			intact = !overlapDetected ? input.get(i).get(0) : intact;
		}

		System.out.println("\n---------------------------------");
		System.out.println(":overlap> " + overlap);

		System.out.println("\n---------------------------------");
		System.out.println(":intact> " + intact);

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

}
