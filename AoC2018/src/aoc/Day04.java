/**
 * 
 */
package aoc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day04 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayList<String> input = new ArrayList<String>();

		HashMap<Integer, int[]> guards = new HashMap<Integer, int[]>();
		Integer currentGuard = -1;
		int startSleep = -1;
		int endSleep = -1;

		try {

			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					throw new Exception();
				}

				input.add(seq);

			}

		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(e);
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}

		Collections.sort(input, new SortByTimestamp());

		for (String seq : input) {
			if (seq.indexOf('#') >= 0) {
				String hlpr = seq.substring(seq.indexOf('#') + 1);
				currentGuard = Integer.valueOf(hlpr.substring(0, hlpr.indexOf(' ')));
				guards.putIfAbsent(currentGuard, new int[60]);
			}

			if (currentGuard >= 0) {
				if (seq.indexOf("falls asleep") >= 0) {
					startSleep = Integer.valueOf(seq.substring(seq.indexOf(':') + 1, seq.indexOf(']')));
				}

				if (seq.indexOf("wakes up") >= 0) {
					endSleep = Integer.valueOf(seq.substring(seq.indexOf(':') + 1, seq.indexOf(']')));

					if (startSleep > -1 && endSleep > -1) {
						int[] currentSleep = guards.get(currentGuard);

						for (int i = startSleep; i <= endSleep; i++) {
							currentSleep[i]++;
						}
					}
				}
			}
		}

		System.out.println("\n---------------------------------");

		int maxAsleepTotal = 0;
		int maxAsleepID = -1;
		int maxAsleepMinute = 0;

		int mostFrequentMinute = -1;
		int mostFrequentID = -1;

		int freqMax = 0;
		for (Integer key : guards.keySet()) {
			System.out.println(":key> " + key);

			int[] sleepTime = guards.get(key);
			System.out.print(":sleepTime> ");
			int max = 0;
			int maxMinute = -1;

			int sum = 0;
			for (int i = 0; i < sleepTime.length; i++) {
				System.out.print("-" + sleepTime[i]);
				if (max < sleepTime[i]) {
					max = sleepTime[i];
					maxMinute = i;
				}
				sum += sleepTime[i];

				if (freqMax < sleepTime[i]) {
					freqMax = sleepTime[i];
					mostFrequentMinute = i;
					mostFrequentID = key;
				}
			}

			if (maxAsleepTotal < sum) {
				maxAsleepTotal = sum;
				maxAsleepID = key;
				maxAsleepMinute = maxMinute;
			}
			System.out.println();

			System.out.println(":maxMinute> " + maxMinute);
			System.out.println(":product> " + (key * maxMinute));
			System.out.println();

			if (maxMinute > maxAsleepMinute) {
				maxAsleepMinute = maxMinute;
				maxAsleepID = key;
			}
		}

		System.out.println("\n---------------------------------");
		System.out.println(":maxAsleepTotal> " + maxAsleepTotal);
		System.out.println(":maxAsleepMinute> " + maxAsleepMinute);
		System.out.println(":maxAsleepID> " + maxAsleepID);
		System.out.println(":product> " + (maxAsleepID * maxAsleepMinute));

		System.out.println("\n---------------------------------");
		System.out.println(":mostFrequentMinute> " + mostFrequentMinute);
		System.out.println(":mostFrequentID> " + mostFrequentID);
		System.out.println(":product> " + (mostFrequentID * mostFrequentMinute));

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

	static class SortByTimestamp implements Comparator<String> {

		public int compare(String line1, String line2) {
			return line1.substring(0, 18).compareTo(line2.substring(0, 18));
		}
	}

}
