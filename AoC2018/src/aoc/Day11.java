
/**
 * 
 */
package aoc;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int serialNo = 5535;
		int[][] powerGrid = new int[300][300];

		for (int x = 0; x < 300; x++) {
			for (int y = 0; y < 300; y++) {
				String cell = String.valueOf(((x + 1 + 10) * (y + 1) + serialNo) * (x + 1 + 10));
				cell = cell.charAt(cell.length() - 3) + "";
				powerGrid[x][y] = Integer.valueOf(cell) - 5;
			}
		}

		String maxTotalPowerCoordinates = "";
		int maxTotalPower = powerGrid[0][0];
		for (int x = 0; x < 297; x++) {
			for (int y = 0; y < 297; y++) {
				int totalPower = 0;
				for (int xframe = x; xframe < x + 3; xframe++) {
					for (int yframe = y; yframe < y + 3; yframe++) {
						totalPower = totalPower + powerGrid[xframe][yframe];
					}
				}
				if (x == 0 && y == 0 || totalPower > maxTotalPower) {
					maxTotalPower = totalPower;
					maxTotalPowerCoordinates = "" + (x + 1) + "," + (y + 1);
				}
			}
		}

		System.out.println("\n---------------------------------");
		System.out.println(":maxTotalPowerCoordinates>" + maxTotalPowerCoordinates);

		String maxTotalPowerCoordinatesSize = "";
		maxTotalPower = powerGrid[0][0];
		for (int size = 0; size < 300; size++) {
			for (int x = 0; x < 300 - size; x++) {
				for (int y = 0; y < 300 - size; y++) {
					int totalPower = 0;
					for (int xframe = x; xframe < x + size + 1; xframe++) {
						for (int yframe = y; yframe < y + size + 1; yframe++) {
							totalPower = totalPower + powerGrid[xframe][yframe];
						}
					}
					if (x == 0 && y == 0 && size == 0 || totalPower > maxTotalPower) {
						maxTotalPower = totalPower;
						maxTotalPowerCoordinatesSize = "" + (x + 1) + "," + (y + 1) + "," + (size + 1);
					}
				}
			}
		}

		System.out.println("\n---------------------------------");
		System.out.println(":maxTotalPowerCoordinatesSize>" + maxTotalPowerCoordinatesSize);

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

}