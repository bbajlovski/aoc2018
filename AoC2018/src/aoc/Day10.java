
/**
 * 
 */
package aoc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayList<String> input = new ArrayList<String>();
		try {

			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					throw new Exception();
				}

				input.add(seq);
			}

		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(e);
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}

		if (input.size() > 0) {
			System.out.println("\n---------------------------------");
			System.out.println(":points>");
			int[] x = new int[input.size()];
			int[] y = new int[input.size()];
			int[] xV = new int[input.size()];
			int[] yV = new int[input.size()];

			for (int i = 0; i < input.size(); i++) {
				String line = input.get(i);
				System.out.println(":line>" + line);
				x[i] = Integer.valueOf(line.substring(line.indexOf("<") + 1, line.indexOf(",")).trim());
				y[i] = Integer.valueOf(line.substring(line.indexOf(",") + 1, line.indexOf(">")).trim());

				line = line.substring(line.indexOf("velocity") + 1);

				xV[i] = Integer.valueOf(line.substring(line.indexOf("<") + 1, line.indexOf(",")).trim());
				yV[i] = Integer.valueOf(line.substring(line.indexOf(",") + 1, line.indexOf(">")).trim());

//				System.out.println(":i / x / y / xV / yV>" + i + "\t" + x[i] + "\t" + y[i] + "\t" + xV[i] + "\t" + yV[i]);
			}

			System.out.println("\n---------------------------------");
			System.out.println(":seconds>");

			for (int seconds = 0; seconds < 10137; seconds++) {
//				if (seconds % 10 == 0) {
				System.out.println("\n---------------------------------");

				Utils.printMatrixRangeWithFields(x, y);
				System.out.println(":second>" + seconds);
//				}

				for (int i = 0; i < input.size(); i++) {
					x[i] = x[i] + xV[i];
					y[i] = y[i] + yV[i];
				}

			}

		}

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

}