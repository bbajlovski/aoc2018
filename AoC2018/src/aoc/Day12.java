
/**
 * 
 */
package aoc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day12 {

	public static long i = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayList<String> plantSeq = new ArrayList<String>();
		ArrayList<String> plantResult = new ArrayList<String>();
		String state = "";
		int end = 0;
		try {

			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					end++;
					if (end > 1) {
						throw new Exception();
					}
				} else {
					if (seq.startsWith("initial")) {
						state = seq.substring(seq.indexOf(":") + 2);
					} else {
						plantSeq.add(seq.substring(0, 5));
						plantResult.add(seq.substring(seq.length() - 1));
					}
				}
			}

		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(e);
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}

		long generations = 50000000000L;
		// long generations = 20L;
		long addLeft = 0L;
		StringBuilder stateBuilder = new StringBuilder(state);
		int repeat = 0;

		System.out.println("\n---------------------------------");
		System.out.println(":generations>" + generations);

		for (long i = 0; i < generations; i++) {

			while (stateBuilder.substring(0, 5).compareTo(".....") != 0) {
				stateBuilder = stateBuilder.insert(0, '.');
				addLeft++;
			}

			while (stateBuilder.substring(stateBuilder.length() - 5).compareTo(".....") != 0) {
				stateBuilder = stateBuilder.append('.');
			}

			StringBuilder newStateBuilder = new StringBuilder("");
			for (int c = 0; c < stateBuilder.length(); c++) {
				newStateBuilder = newStateBuilder.append('.');
			}

			for (int c = 0; c < stateBuilder.length() - 4; c++) {
				String pot = stateBuilder.substring(c, c + 5);
				for (int j = 0; j < plantSeq.size(); j++) {
					if (pot.compareTo(plantSeq.get(j)) == 0) {
						newStateBuilder = newStateBuilder.replace(c + 2, c + 3, plantResult.get(j));

						break;
					}
				}
			}

			while (newStateBuilder.substring(0, 1).compareTo(".") == 0) {
				newStateBuilder = newStateBuilder.delete(0, 1);
				addLeft--;
			}

			while (newStateBuilder.substring(newStateBuilder.length() - 1).compareTo(".") == 0) {
				newStateBuilder = newStateBuilder.deleteCharAt(newStateBuilder.length() - 1);
			}

			while (stateBuilder.substring(0, 1).compareTo(".") == 0) {
				stateBuilder = stateBuilder.delete(0, 1);
			}

			while (stateBuilder.substring(stateBuilder.length() - 1).compareTo(".") == 0) {
				stateBuilder = stateBuilder.deleteCharAt(stateBuilder.length() - 1);
			}

			repeat = stateBuilder.toString().compareTo(newStateBuilder.toString()) == 0 ? repeat + 1 : 0;
			stateBuilder = newStateBuilder;

			if (repeat >= 5) {
				addLeft = addLeft - (generations - i) + 1;
				break;
			}

		}

		long potSum = 0;
		for (int i = 0; i < stateBuilder.length(); i++) {
			if (stateBuilder.charAt(i) == '#') {
				potSum = potSum + (i - addLeft);
			}
		}

		System.out.println("\n---------------------------------");
		System.out.println(":potSum>" + potSum);

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

}