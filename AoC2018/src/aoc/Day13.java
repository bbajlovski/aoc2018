/**
 * 
 */
package aoc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day13 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayList<String> input = new ArrayList<String>();

		try {
			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					throw new Exception();
				}
				input.add(seq);

			}
		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}
		
		/*
		 * -1: X (crash)
		 * 0: empty
		 * 1: -
		 * 2: |
		 * 3: /
		 * 4: \
		 * 5: +
		 * 6: >
		 * 7: <
		 * 8: ^
		 * 9: v
		 */
		int[][] map = null; 
		
		if (input!=null && input.size()>0) {
			int ySize = input.size();
			int xSize = input.get(0).length();
			map = new int[ySize][xSize];
			
			for (int y = 0; y<ySize; y++) {
				for (int x = 0; x<xSize; x++) {
					char c = input.get(y).charAt(x);
					switch (c) {
						case '-': {
							map[y][x] = 1;
							break;
						}
						case '|': {
							map[y][x] = 2;
							break;
						}
						case '/': {
							map[y][x] = 3;
							break;
						}
						case '\\': {
							map[y][x] = 4;
							break;
						}
						case '+': {
							map[y][x] = 5;
							break;
						}
						case '>': {
							map[y][x] = 6;
							break;
						}
						case '<': {
							map[y][x] = 7;
							break;
						}
						case '^': {
							map[y][x] = 8;
							break;
						}
						case 'v': {
							map[y][x] = 9;
							break;
						}
					}
				}
			}
		}
		System.out.println("\n---------------------------------");
		System.out.println(":map>");
		Utils.printMatrix13(map);
		
		boolean bug = false;
		int crash = 0;
		int crashX = -1;
		int crashY = -1;
		int nCarts = countCarts(map);
		int lastCartX = -1;
		int lastCartY = -1;
		int[][] newMap =Utils.cloneArrayOfArrays(map);
		while (nCarts>1) {
			for (int y = 0; y<map.length; y++) {
				for (int x = 0; x<map[y].length; x++) {

					if (map[y][x] > 5) {
						int lastValue = map[y][x];						
						int lastDirection = lastValue % 10;
						int lastMark = ((lastValue - lastDirection) / 10) % 10;
						int lastTurn = (lastValue - lastMark * 10 - lastDirection) / 100;
						int newValue = 0;
						int newX = x;
						int newY = y;
						int newMark = 0;
						int newTurn = lastTurn;
						int newDirection = 0;
						int crashDirection = 0;
						
						
						
						if (lastDirection == 6) {
							newX = newX + 1;
							
							if (newMap[newY][newX] > 5) {
								crash++;
								if (crash == 1) {
									crashY = newY;
									crashX = newX;
								}
								crashDirection = ((newMap[newY][newX] - newMap[newY][newX] % 10) / 10) % 10;
							}
							
							if (map[newY][newX] == 1) {
								newMark = map[newY][newX];
								newDirection = 6;								
							}
							
							if (map[newY][newX] == 3) {
								newMark = map[newY][newX];
								newDirection = 8;
							}
							
							if (map[newY][newX] == 4) {
								newMark = map[newY][newX];
								newDirection = 9;
							}
							
							if (map[newY][newX] == 5) {
								newTurn = lastTurn + 1 > 2 ? 0 : lastTurn + 1;
								newMark = map[newY][newX];								
								newDirection = lastTurn == 0 ? 8 : (lastTurn == 1 ? 6 : (lastTurn == 2 ? 9 : lastTurn));
							}

							lastMark = lastMark == 0 ? 1 : lastMark;
						}
						
						if (lastDirection == 7) {
							newX = newX - 1;
							
							if (newMap[newY][newX] > 5) {
								crash++;
								if (crash == 1) {
									crashY = newY;
									crashX = newX;
								}
								crashDirection = ((newMap[newY][newX] - newMap[newY][newX] % 10) / 10) % 10;
							}
							
							if (map[newY][newX] == 1) {
								newMark = map[newY][newX];
								newDirection = 7;								
							}
							
							if (map[newY][newX] == 3) {
								newMark = map[newY][newX];
								newDirection = 9;
							}
							
							if (map[newY][newX] == 4) {
								newMark = map[newY][newX];
								newDirection = 8;
							}
							
							if (map[newY][newX] == 5) {
								newTurn = lastTurn + 1 > 2 ? 0 : lastTurn + 1;
								newMark = map[newY][newX];								
								newDirection = lastTurn == 0 ? 9 : (lastTurn == 1 ? 7 : (lastTurn == 2 ? 8 : lastTurn));
							}
							
							lastMark = lastMark == 0 ? 1 : lastMark;
						}
						
						if (lastDirection == 8) {
							newY = newY - 1;
							
							if (newMap[newY][newX] > 5) {
								crash++;
								if (crash == 1) {
									crashY = newY;
									crashX = newX;
								}
								crashDirection = ((newMap[newY][newX] - newMap[newY][newX] % 10) / 10) % 10;
							}
							
							if (map[newY][newX] == 2) {
								newMark = map[newY][newX];
								newDirection = 8;								
							}
							
							if (map[newY][newX] == 3) {
								newMark = map[newY][newX];
								newDirection = 6;
							}
							
							if (map[newY][newX] == 4) {
								newMark = map[newY][newX];
								newDirection = 7;
							}
							
							if (map[newY][newX] == 5) {
								newTurn = lastTurn + 1 > 2 ? 0 : lastTurn + 1;
								newMark = map[newY][newX];								
								newDirection = lastTurn == 0 ? 7 : (lastTurn == 1 ? 8 : (lastTurn == 2 ? 6 : lastTurn));
							}
							
							lastMark = lastMark == 0 ? 2 : lastMark;
						}
						
						if (lastDirection == 9) {
							newY = newY + 1;
							
							if (newMap[newY][newX] > 5) {
								crash++;
								if (crash == 1) {
									crashY = newY;
									crashX = newX;
								}
								crashDirection = ((newMap[newY][newX] - newMap[newY][newX] % 10) / 10) % 10;
							}
							
							if (map[newY][newX] == 2) {
								newMark = map[newY][newX];
								newDirection = 9;								
							}
							
							if (map[newY][newX] == 3) {
								newMark = map[newY][newX];
								newDirection = 7;
							}
							
							if (map[newY][newX] == 4) {
								newMark = map[newY][newX];
								newDirection = 6;
							}
							
							if (map[newY][newX] == 5) {
								newTurn = lastTurn + 1 > 2 ? 0 : lastTurn + 1;
								newMark = map[newY][newX];								
								newDirection = lastTurn == 0 ? 6 : (lastTurn == 1 ? 9 : (lastTurn == 2 ? 7 : lastTurn));
							}
							lastMark = lastMark == 0 ? 2 : lastMark;
						}
						
						newValue = crashDirection > 0 ? crashDirection : (newTurn * 100 + newMark * 10 + newDirection);
//						if (newValue == 0) {//						
//							System.out.println("\nBUG---------------------------------");
//							System.out.println(":x,y>" + x + "," + y);
//							System.out.println(":lastValue>" + newMap[y][x]);
//							Utils.printMatrix13(newMap);
//							bug = true;
//							break;
//						}

						newMap[newY][newX] = newValue;
						newMap[y][x] = lastMark;
						

//						System.out.println("\n---------------------------------");
//						System.out.println(":x,y>" + x + "," + y);
//						System.out.println(":map>");
//						Utils.printMatrix13(map);
//						System.out.println(":newMap>");
//						Utils.printMatrix13(newMap);
						

					}
				}
				if (bug) {
					break;
				}
			}

			if (bug) {
				break;
			}
			
			nCarts = countCarts(newMap);

			System.out.println(":newMap>");
			Utils.printMatrix13(newMap);
			
			map = Utils.cloneArrayOfArrays(newMap);
		}
		
		System.out.println("\n---------------------------------");
		System.out.println(":firstCrashCoordinates>" + crashX+","+crashY);
		
		if (nCarts <=1) {
			for(int yN=0; yN<newMap.length; yN++) {
				  for(int xN=0; xN<newMap[yN].length; xN++) {
					  int value = newMap[yN][xN] % 10;
					  if (value>=6 && value<=9) {
						  lastCartY = yN;
						  lastCartX = xN;
					  }
				  }
			}
		}
		System.out.println("\n---------------------------------");
		System.out.println(":lastCartCoordinates>" + lastCartX+","+lastCartY);

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}
	
	public static int countCarts (int[][] map) {
		int nCarts = 0;
		
		for(int i=0; i<map.length; i++) {
			  for(int j=0; j<map[i].length; j++) {
				  int value = map[i][j] % 10;
				  if (value>=6 && value<=9) {
					  nCarts++;
				  }
			  }
		}
		
		return nCarts;
	}
}
