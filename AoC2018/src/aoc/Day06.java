/**
 * 
 */
package aoc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day06 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayList<int[]> input = new ArrayList<int[]>();
		int maxX = 0;
		int maxY = 0;
		int[][] area = null;
		int[][] markedArea = null;
		int[][] sumArea = null;
		try {

			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					throw new Exception();
				}

				int[] current = new int[3];
				current[1] = (Integer.valueOf(seq.substring(0, seq.indexOf(",")).trim()));
				current[0] = (Integer.valueOf(seq.substring(seq.indexOf(",") + 1).trim()));
				input.add(current);

				maxX = maxX > current[0] ? maxX : current[0];
				maxY = maxY > current[1] ? maxY : current[1];

			}

		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(e);
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}

		maxX++;
		maxY++;

		area = new int[maxX][maxY];
		markedArea = new int[maxX][maxY];
		sumArea = new int[maxX][maxY];

		// init areas
		for (int x = 0; x < maxX; x++) {
			for (int y = 0; y < maxY; y++) {
				area[x][y] = -1;
				markedArea[x][y] = -1;
				sumArea[x][y] = 0;
			}
		}

		
		
		for (int i = 0; i < input.size(); i++) {
			int x = input.get(i)[0];
			int y = input.get(i)[1];
			area[x][y] = i;
		}

		// marking and summing the distances
		System.out.println("\n---------------------------------");
		for (int x = 0; x < maxX; x++) {
			for (int y = 0; y < maxY; y++) {
				int minDistance = -1;
				int id = -1;

				for (int i = 0; i < input.size(); i++) {
					int xA = input.get(i)[0];
					int yA = input.get(i)[1];
					int distance = Math.abs(xA - x) + Math.abs(yA - y);
					if (minDistance > distance || minDistance < 0) {
						minDistance = distance;
						id = area[xA][yA];
					} else {
						if (minDistance == distance) {
							minDistance = distance;
							id = -2;
						}
					}
					
					sumArea[x][y] = sumArea[x][y] + distance;
				}

				markedArea[x][y] = id;

			}
		}

		// marking the infinite
		for (int x = 0; x < maxX; x++) {
			for (int y = 0; y < maxY; y++) {
				if ((x==0 || x==maxX-1 || y==0 || y==maxY) && markedArea[x][y]>=0) {
					
					int id = markedArea[x][y];
					for (int xM = 0; xM < maxX; xM++) {
						for (int yM = 0; yM < maxY; yM++) {
							if (markedArea[xM][yM] == id) {
								markedArea[xM][yM] = -3;
							}
						}
					}
				}
			}
		}

		System.out.println(":markedArea>");
		Utils.printMatrix(markedArea);		

		System.out.println(":sumArea>");
		Utils.printMatrix(sumArea);
		
		// counting non-infinite
		for (int x = 0; x < maxX; x++) {
			for (int y = 0; y < maxY; y++) {
				if (markedArea[x][y] >= 0) {
					int id = markedArea[x][y];
					input.get(id)[2] = input.get(id)[2] + 1; 
				}
			}
		}
		
		int maxArea = 0;
		for(int[] current : input) {
			if (maxArea < current[2]) {
				maxArea = current[2];
			}
		}

		System.out.println("\n---------------------------------");
		System.out.println(":maxArea>" + maxArea);
		
		// checking sums
		int limitSumArea=0;
		for (int x = 0; x < maxX; x++) {
			for (int y = 0; y < maxY; y++) {
				limitSumArea = sumArea[x][y] < 10000 ? limitSumArea+1 : limitSumArea;
			}
		}
		
		System.out.println("\n---------------------------------");
		System.out.println(":limitSumArea>" + limitSumArea);		
		
		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}


}