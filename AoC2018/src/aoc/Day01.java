/**
 * 
 */
package aoc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayList<Integer> input = new ArrayList<Integer>();
		Integer sum = 0;

		try {
			while (scanner.hasNextLine()) {
				Integer freq = Integer.valueOf(scanner.nextLine());
				input.add(freq);
				sum = sum + freq;
			}
		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(":last input detected>");
		}

		scanner.close();

		System.out.println("\n---------------------------------");
		System.out.println(":sum> " + sum);

		sum = 0;
		boolean foundDoubleFreq = false;
		Integer doubleFreq = sum;
		ArrayList<Integer> sumArray = new ArrayList<Integer>();
		sumArray.add(sum);
		int n = 0;
		while (!foundDoubleFreq) {

			sum = sum + input.get(n);
			if (sumArray.contains(sum)) {
				foundDoubleFreq = true;
				doubleFreq = sum;
			} else {
				sumArray.add(sum);
				n++;
				n = n >= input.size() ? 0 : n;
			}
		}

		System.out.println("\n---------------------------------");
		System.out.println(":firstDouble> " + doubleFreq);

		
		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

}
