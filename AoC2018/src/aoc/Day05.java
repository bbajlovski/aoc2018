/**
 * 
 */
package aoc;

import java.util.Scanner;
import java.util.Stack;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day05 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String polymer = null;

		try {

			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					throw new Exception();
				}
				polymer = seq;
			}

		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(e);
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}

		char[] polymerUnits = polymer.toCharArray();
		int len = burnPolymerWithStack(polymerUnits, null);

		System.out.println("\n---------------------------------");
		System.out.println(":polymerUnits> " + len);

		long startTime = System.currentTimeMillis();
		int min = polymer.length();
		for (char unit = 'a'; unit < ('z' + 1); unit++) {
			polymerUnits = polymer.toCharArray();
			int lenTest = burnPolymerWithStack(polymerUnits, unit);

			min = min > lenTest ? lenTest : min;
		}
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("\n---------------------------------");
		System.out.println(":minimalUnits> " + min);
		System.out.println(":elapsedTime> " + elapsedTime + "ms");

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

	public static int burnPolymerWithStack(char[] polymerUnits, Character ignore) {
		long startTime = System.currentTimeMillis();

		Stack<Character> temp = new Stack<Character>();

		for (int i = 0; i < polymerUnits.length; i++) {
			if (ignore == null || Character.toLowerCase(ignore) != Character.toLowerCase(polymerUnits[i])) {
				if (temp.empty()) {
					temp.push(polymerUnits[i]);
				} else {
					char tempLast = temp.pop();
					char current = polymerUnits[i];
					if (!(Character.toLowerCase(tempLast) == Character.toLowerCase(current) && tempLast != current)) {
						temp.push(tempLast);
						temp.push(current);
					}
				}
			}
		}

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		if (ignore == null) {
			System.out.println("\n---------------------------------");
			System.out.println(":elapsedTime> " + elapsedTime + "ms");
		}

		return temp.size();
	}
}
