
/**
 * 
 */
package aoc;

import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day08 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int[] input = null;

		try {

			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					throw new Exception();
				}

				String[] inputStr = seq.split(" ");
				if (inputStr != null && inputStr.length > 0) {
					input = new int[inputStr.length];
					for (int i = 0; i < inputStr.length; i++) {
						input[i] = Integer.valueOf(inputStr[i]);
					}
				}
			}

		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(e);
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}

		Node8 n = new Node8(0, input);


		System.out.println("\n---------------------------------");
		System.out.println(":checksum>" + n.checksum());

		System.out.println("\n---------------------------------");
		System.out.println(":value>" + n.value());

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

}

class Node8 {
	private int[] metadata = null;
	private Node8[] children = null;
	private int startPosition = -1;
	private int endPosition = -1;
	
	public Node8(int position, int[] input) {
		this.startPosition = position;
		int newStartPosition = startPosition+2;		
		if (input[this.startPosition] > 0) {
			children = new Node8[input[this.startPosition]];

			for (int i = 0; i<children.length; i++) {				
				children[i] = new Node8(newStartPosition, input);
				newStartPosition = children[i].getEndPosition()+1;
			}
		}
		
		if (input[this.startPosition+1] > 0) {
			metadata = new int[input[this.startPosition+1]];
			for (int i = 0; i<metadata.length; i++) {
				metadata[i] = input[newStartPosition];
				newStartPosition++;
			}
		}
		endPosition = newStartPosition - 1;
	}
	
	public int checksum() {
		int sum = 0;
		
		for (int m : metadata) {
			sum = sum + m;
		}
		
		if (children != null && children.length>0) {
			for (Node8 n : children) {
				sum = sum + n.checksum(); 
			}
		}
		return sum;
	}
	
	public int value() {
		int sum = 0;
		
		if (children == null || children.length==0) {
			for (int m : metadata) {
				sum = sum + m;
			}
		} else {		
			if (children != null && children.length>0) {
				for (int m : metadata) {
					if (m <= children.length && m>0) {
						sum = sum + children[m-1].value();
					}
				}
			}
		}
		return sum;
	}
	
	public int getEndPosition() {
		return endPosition;
	}
	
	public int getStartPosition() {
		return startPosition;
	}
}
