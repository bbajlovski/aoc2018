/**
 * 
 */
package aoc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day07 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayList<Node7> input = new ArrayList<Node7>();

		try {

			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					throw new Exception();
				}

				String predcessorId = seq.substring(5, seq.indexOf("must")).trim();
				String nodeId = seq.substring(seq.indexOf("step") + 4, seq.indexOf("can")).trim();

				Node7 p = null;
				Node7 n = null;
				boolean pFound = false;
				boolean nFound = false;

				for (Node7 iNode : input) {

					if (iNode.getId().compareTo(predcessorId) == 0 && !pFound) {
						p = iNode;
						pFound = true;
					}

					if (iNode.getId().compareTo(nodeId) == 0 && !nFound) {
						n = iNode;
						nFound = true;
					}
				}

				if (!pFound || p == null) {
					p = new Node7(predcessorId);
					input.add(p);
				}

				if (!nFound || n == null) {
					n = new Node7(nodeId);
					input.add(n);
				}

				if (!n.containsPredcessor(p.getId())) {
					n.addPredcessor(p);
				}
			}

		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(e);
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}

		int inputSize = input.size();

		int seconds = 0;
		String output = "";

		// set workers = 1 for part 1 of the task
		// set workers = 2 for sample input of part 2
		// set workers = 5 for part 2 of the task
		int nWorkers = 5;

		// set offset = 0 for part 1 of the task
		// set offset = 0 for sample input of part 2
		// set offset = 60 for part 2 of the task
		int offset = 60;

		int[] workersETA = new int[nWorkers];
		String[] workersTask = new String[nWorkers];
		for (int i = 0; i < nWorkers; i++) {
			workersTask[i] = null;
		}

		System.out.println("\n---------------------------------");
		System.out.println(":steps>");

		ArrayList<Node7> temp = new ArrayList<Node7>();
		for (Node7 iNode : input) {
			if (!iNode.hasPredcessors()) {
				temp.add(iNode);
			}
		}

		for (Node7 tempNode : temp) {

			input.remove(tempNode);
		}

		temp.sort(new SortById());

		while (inputSize > output.length() || output.length() == 0) {

			System.out.print(seconds + "\t");

			int iWorker = 0;
			while (iWorker < nWorkers) {

				if (workersETA[iWorker] == 0) {

					if (workersTask[iWorker] != null) {
						for (Node7 iNode : input) {
							if (iNode.containsPredcessor(workersTask[iWorker])) {
								iNode.removePredcessor(workersTask[iWorker]);
							}
						}
						output = output + workersTask[iWorker];
						workersTask[iWorker] = null;
						for (Node7 iNode : input) {
							if (!iNode.hasPredcessors()) {
								temp.add(iNode);
							}
						}

						for (Node7 tempNode : temp) {

							input.remove(tempNode);
						}

						temp.sort(new SortById());
						workersTask[iWorker] = null;
						iWorker = -1;
					} else {

						if (!temp.isEmpty()) {
							Node7 n = temp.get(0);
							temp.remove(n);
							workersTask[iWorker] = n.getId();
							workersETA[iWorker] = n.ttl(offset);
						}
					}
				}

				iWorker++;
			}

			iWorker = 0;
			while (iWorker < nWorkers) {
				System.out.print((workersTask[iWorker] == null ? "." : workersTask[iWorker]) + "\t");
				workersETA[iWorker] = workersETA[iWorker] > 0 ? workersETA[iWorker] - 1 : workersETA[iWorker];
				iWorker++;
			}
			System.out.println(output);
			seconds++;
		}

		seconds--;

		System.out.println("\n---------------------------------");
		System.out.println(":nWorkers>" + nWorkers);
		System.out.println(":output>" + output);
		System.out.println(":seconds>" + seconds);

		// multiple workers

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

}

class Node7 {
	private ArrayList<Node7> predcessors = new ArrayList<Node7>();
	private String id = null;

	public Node7(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void addPredcessor(Node7 n) {
		if (predcessors == null) {
			predcessors = new ArrayList<Node7>();
		}

		predcessors.add(n);
		predcessors.sort(new SortById());
	}

	public void removePredcessor(Node7 n) {
		predcessors.remove(n);
	}

	public void removePredcessor(String id) {
		Node7 n = null;
		for (Node7 p : predcessors) {
			if (p.getId().compareTo(id) == 0) {
				n = p;
				break;
			}
		}

		if (n != null) {
			removePredcessor(n);
		}
	}

	public boolean hasPredcessors() {
		return predcessors == null || predcessors.size() > 0;
	}

	public boolean containsPredcessor(String s) {
		boolean found = false;

		for (Node7 n : predcessors) {
			found = found || n.getId().compareTo(s) == 0;
		}

		return found;
	}

	public int ttl(int offset) {
		int time = id.charAt(0) - 'A' + 1 + offset;

		return time;
	}
}

class SortById implements Comparator<Node7> {

	public int compare(Node7 n1, Node7 n2) {
		return n1.getId().compareTo(n2.getId());
	}
}