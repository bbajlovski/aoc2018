package aoc;

public class Utils {
	public static void printMatrix(int[][] matrix) {
		for (int x = 0; x < matrix.length; x++) {
			for (int y = 0; y < matrix[x].length; y++) {
				System.out.print(" \t " + matrix[x][y]);
			}
			System.out.println();
		}
	}
	
	public static void printMatrix13(int[][] matrix) {
		for (int x = 0; x < matrix.length; x++) {
			for (int y = 0; y < matrix[x].length; y++) {
				char c = ' ';
				int value = matrix[x][y] % 10;
				switch (value) {
					case -1: {
						c = 'X';
						break;
					}
					case 1: {
						c = '-';
						break;
					}
					case 2: {
						c = '|';
						break;
					}
					case 3: {
						c = '/';
						break;
					}
					case 4: {
						c = '\\';
						break;
					}
					case 5: {
						c = '+';
						break;
					}
					case 6: {
						c = '>';
						break;
					}
					case 7: {
						c = '<';
						break;
					}
					case 8: {
						c = '^';
						break;
					}
					case 9: {
						c = 'v';
						break;
					}
				}
				System.out.print(c);
			}
			System.out.println();
		}
	}

	public static void printMatrixRangeWithFields(int[] xFields, int[] yFields) {
		int[] xMinMax = minAndMax(xFields);
		int[] yMinMax = minAndMax(yFields);

		int xSize = xMinMax[1] - xMinMax[0] + 1;
		int ySize = yMinMax[1] - yMinMax[0] + 1;
		if (ySize < 140) {
			byte[][] matrix = new byte[xSize][ySize];

			for (int i = 0; i < xFields.length; i++) {
				int x = xFields[i] - xMinMax[0];
				int y = yFields[i] - yMinMax[0];
				matrix[x][y] = 1;
			}

			for (int y = 0; y < matrix[0].length; y++) {
				for (int x = 0; x < matrix.length; x++) {
					matrix[x][y] = matrix[x][y] == 0 ? (byte) 0 : (byte) 1;
					System.out.print((matrix[x][y] == 1 ? "#" : "."));
				}
				System.out.println();
			}
		}
		System.out.println(":size> " + xSize + "\t" + ySize);
	}

	public static int[] minAndMax(int[] list) {
		int[] result = { 0, 0 };

		for (int n : list) {
			result[0] = result[0] > n ? n : result[0];
			result[1] = result[1] < n ? n : result[1];
		}

		return result;
	}

	public static int[][] cloneArrayOfArrays(int[][] source) {
		int [][] destination = new int[source.length][source[0].length];
		for(int i=0; i<source.length; i++)
		  for(int j=0; j<source[i].length; j++)
			  destination[i][j]=source[i][j];
		
		return destination;
	}
}
