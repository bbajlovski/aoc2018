/**
 * 
 */
package aoc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Branislav Bajlovski
 *
 */
public class Day02 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		ArrayList<String> input = new ArrayList<String>();
		int doubles = 0;
		int tripples = 0;

		try {
			while (scanner.hasNextLine()) {
				String seq = scanner.nextLine();

				if (seq.length() == 0) {
					throw new Exception();
				}
				input.add(seq);

				boolean hasDouble = false;
				boolean hasTripple = false;
				long count = 0;

				for (char c : seq.toCharArray()) {
					count = seq.chars().filter(ch -> ch == c).count();
					hasDouble = hasDouble || (count == 2);
					hasTripple = hasTripple || (count == 3);
				}

				doubles = doubles + (hasDouble ? 1 : 0);
				tripples = tripples + (hasTripple ? 1 : 0);
			}
		} catch (Exception e) {
			System.out.println("\n---------------------------------");
			System.out.println(":last input detected>");
		} finally {

			scanner.close();
		}

		System.out.println("\n---------------------------------");
		System.out.println(":doubles> " + doubles);
		System.out.println(":tripples> " + tripples);
		System.out.println(":multiplication> " + (doubles * tripples));

		int seqWord = -1;
		int firstDiff = -1;
		for (int i = 0; i < input.size(); i++) {

			for (int j = i + 1; j < input.size(); j++) {
				int diff = 0;
				char[] iId = input.get(i).toCharArray();
				char[] jId = input.get(j).toCharArray();
				firstDiff = -1;
				if (iId.length == jId.length) {
					for (int k = 0; k < iId.length; k++) {
						if (iId[k] != jId[k]) {
							diff++;
							firstDiff = (firstDiff == -1) ? k : firstDiff;
						}
					}
				}

				if (diff == 1) {
					seqWord = i;
					break;
				}
			}
			
			if (seqWord>-1) {
				break;
			}
		}

		String commonLetters = null;
		if (seqWord > -1 && firstDiff > -1) {
			commonLetters = input.get(seqWord).substring(0, firstDiff) + input.get(seqWord).substring(firstDiff + 1);
		}

		System.out.println("\n---------------------------------");
		System.out.println(":firstDiff> " + firstDiff);
		System.out.println(":seqWord> " + seqWord);
		System.out.println(":commonLetters> " + commonLetters);

		System.out.println("\n---------------------------------");
		System.out.println(":bye>");
	}

}
